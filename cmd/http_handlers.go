package main

import (
	"errors"
	"net"
	"net/http"
	"sync"

	"gitlab.com/leolab/go/tools/serverec"
)

func serveHTTP(wg *sync.WaitGroup) *http.Server {
	srv := &http.Server{}
	http.Handle("/res/", http.FileServer(http.FS(files)))
	http.HandleFunc("/", handleRequest)

	go func() {
		defer wg.Done()
		var err error
		var slst net.Listener

		if serveSock != "" {
			slst, err = net.Listen("unix", serveSock)
			if err != nil {
				log.Error("Unix socket error: " + err.Error())
				chExit <- 1
				return
			}
		} else {
			slst, err = net.Listen("tcp", serveAddr)
			if err != nil {
				log.Error("TCP socket error: " + err.Error())
				chExit <- 1
				return
			}
		}

		if err := srv.Serve(slst); err != nil {
			log.Error("Server error: " + err.Error())
			chExit <- 1
			return
		}
	}()
	return srv
}

func handleRequest(w http.ResponseWriter, r *http.Request) {
	log.Request("Request from " + r.RemoteAddr + " for " + r.RequestURI)
	sr, err := serverec.GetServeRec(w, r)
	if err != nil {
		log.Error(err.Error())
		serve500(w, r, err)
		return
	}
	sr.Data["session"], err = ses.Get(sr.W, sr.R)
	if err != nil {
		log.Error(err.Error())
		serve500(w, r, err)
	}
	err = serveRequest(sr)
	if err != nil {
		serve500(w, r, err)
	}
}

func serve500(w http.ResponseWriter, r *http.Request, err error) {
	http.Error(w, err.Error(), 500)
}

func serveRequest(sr *serverec.ServeRec) error {
	return errors.New("Not implemented")
}
