package main

import (
	"context"
	"fmt"
	"io/fs"
	"os"
	"os/signal"
	"strconv"
	"sync"
	"time"

	"gitlab.com/leolab/go/leotools"
	"gitlab.com/leolab/go/logger"
	"gitlab.com/leolab/go/session"
)

var files fs.FS

//var log leotools.Logger
var log *logger.Logger
var ses *session.SessionStore

// Переменные конфигурации
var (
	servePath string // Путь к файлам сайта
	serveSock string // Сокет
	serveAddr string // Порт (адрес)

	wg     = &sync.WaitGroup{}
	chProc = make(chan bool)
	chErr  = make(chan error)
	chSig  = make(chan os.Signal)
	chExit = make(chan int)
)

func main() {
	var ok bool
	var err error

	args, _ /*cmds*/ := leotools.ParseArgs()
	if servePath, ok = args["serve.path"]; !ok {
		servePath = leotools.GetCWD() + "/static"
	}
	if serveSock, ok = args["serve.sock"]; !ok {
		serveSock = ""
	}
	if serveAddr, ok = args["serve.addr"]; !ok {
		if serveSock != "" {
			serveAddr = ":8080"
		}
	}

	//== Logger >>
	lc := &logger.LoggerConfig{}
	if lc.LoggerPath, ok = args["log.store"]; !ok {
		lc.DataStore = ""
	}
	if lc.LoggerPath, ok = args["log.path"]; !ok {
		lc.LoggerPath = ""
	}
	if _, ok = args["log.dbg"]; ok {
		lc.Dbg = true
	}
	log, err = logger.NewLogger(lc)
	if err != nil {
		fmt.Println("Logger error: ", err)
		os.Exit(1)
	}
	log.Levels.Cons.Set(logger.LoggerLevelAll...)
	log.Levels.Data.Set(logger.LoggerLevelAll...)
	log.Levels.File.Set(logger.LoggerLevelAll...)
	log.Info("Starting...")
	//<< Logger ==

	//== chErr receiver >>
	wg.Add(1)
	go func() {
		defer wg.Done()
		for {
			select {
			case err := <-chErr:
				if log != nil {
					log.Error(err.Error(), err)
				} else {
					fmt.Println(err.Error())
				}
			case <-chProc:
				return
			}
		}
	}()
	//<< chErr receiver ==

	//== Session >>
	sc := &session.SessionCfg{
		ChanClose: nil,
		ChanError: nil,
		WG:        nil,
	}
	if sc.SessionKey, ok = args["session.key"]; !ok {
		sc.SessionKey = leotools.GetBinName() + "_session_key"
	}
	if sdt, ok := args["session.time"]; ok {
		sc.SessionDuration, err = time.ParseDuration(sdt)
		if err != nil {
			log.Fatal(err.Error())
			os.Exit(1)
		}
	} else {
		sc.SessionDuration = 30 * time.Second
	}
	sc.ChanClose = chProc
	sc.ChanError = chErr
	sc.WG = wg
	wg.Add(1)
	ses, err = session.NewSessionStore(sc)
	if err != nil {
		log.Fatal(err.Error())
		os.Exit(1)
	}
	log.Info("Sessions started")
	//<< Session ==

	//== OS Signals >>
	signal.Notify(chSig, os.Interrupt)
	wg.Add(1)
	go func() {
		defer wg.Done()
		select {
		case sig := <-chSig:
			if log != nil {
				log.Notify("Received signal: " + sig.String())
			}
			chExit <- 0
			return
		case <-chProc:
			return
		}
	}()
	//<< OS Signals ==

	//== Serve FS >>
	if !fs.ValidPath(servePath) {
		log.Fatal("Not valid path to serve: " + servePath)
		os.Exit(1)
	}
	files = os.DirFS(servePath)
	//<< Serve FS ==

	//== HTTP-Server >>
	wg.Add(1)
	srv := serveHTTP(wg)
	//<< HTTP-Server ==

	excode := <-chExit
	log.Info("Shutdown with code: " + strconv.Itoa(excode))
	err = srv.Shutdown(context.TODO())
	if err != nil {
		log.Error("Error shutdown server: " + err.Error())
	}
	close(chProc)
	wg.Wait()
	log.Info("Finished")
	os.Exit(excode)

}
