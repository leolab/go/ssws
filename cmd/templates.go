package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"strings"
	"text/template"
)

var tplFuncMap template.FuncMap = template.FuncMap{}

func init() {
	tplFuncMap["include"] = func(file string, data map[string]interface{}) string { return getContent(file, data) }
}

func getContent(file string, data map[string]interface{}) string {
	f, err := files.Open(file + ".html")
	if err != nil {
		//log.Error(err)
		return err.Error()
	}

	d, err := io.ReadAll(f)
	if d[0] == '#' {
		b := bytes.NewBuffer(d)
		s, err := b.ReadString('\n')
		if err != nil {
			return err.Error()
		}
		sa := strings.Split(strings.Trim(s, "#\n"), " ")
		switch sa[0] {
		case "extends":
			data["Content"] = parseTemplate(b.Bytes(), data)
			return getContent(sa[1], data)
		default:
			err := errors.New("Unknown argument: " + sa[0])
			return err.Error()
		}
	} else {

		if err != nil {
			//log.Error(err)
			return err.Error()
		}
		t, err := template.New(file).Funcs(tplFuncMap).Parse(string(d))
		if err != nil {
			return err.Error()
		}
		var tw bytes.Buffer
		err = t.Execute(&tw, data)
		if err != nil {
			return err.Error()
		}
		return tw.String()
	}
}

func parseTemplate(tpl []byte, data interface{}) string {
	t, err := template.New("template").Funcs(tplFuncMap).Parse(string(tpl))
	if err != nil {
		//log.Error(err)
		return err.Error()
	}
	var tw bytes.Buffer
	err = t.Execute(&tw, data)
	if err != nil {
		//log.Error(err)
		return err.Error()
	}
	return tw.String()
}

func getData(file string) (data map[string]interface{}) {
	f, err := files.Open(file + ".json")
	if err != nil {
		//log.Error(err)
		return map[string]interface{}{"Error": err}
	}
	rb, err := io.ReadAll(f)
	if err != nil {
		//log.Error(err)
		return map[string]interface{}{"Error": err}
	}
	err = json.Unmarshal(rb, &data)
	if err != nil {
		//log.Error(err)
		return map[string]interface{}{"Error": err}
	}
	return data
}

func serve500Error(w http.ResponseWriter, r *http.Request, err error) {
	http.Error(w, err.Error(), 500)
}
