module src/main

go 1.16

require (
	gitlab.com/leolab/go/leotools v0.2.1
	gitlab.com/leolab/go/logger v0.1.4
	gitlab.com/leolab/go/session v0.1.0
	gitlab.com/leolab/go/tools/serverec v0.1.0
)
