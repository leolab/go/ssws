# SSWS
Simple Static Web Server

Простой веб-сервер для обработки статических файлов

Предназначается в первую очередь для развертывания локального веб-сервера для верстальщиков.

## Параметры запуска

- serve.path
- serve.sock
- serve.addr
- log.store
- log.path
- log.dbg


## Страницы
`/index.html`
```html
<!doctype html>

<html>
    {{include "tpl/head"}}
    <body>
        {{include "tpl/header"}}
<main>
    <h1>Page contents</h1>
</main>
        {{include "tpl/footer"}}
    </body>
</html>
```

`/tpl/head.html`
```html
<head>
    ...
</head>
```

`/tpl/header.html`
```html
<header>
    <p>Page header</p>
</header>
```

`/tpl/footer.html`
```html
<footer>
    <p>Page footer</p>
</footer>
```

## Страницы с общим шаблоном (Featured)
`/index.html`
```html
#extends tpl/layout

<main>
    <h1>Page contents</h1>
</main>
```

